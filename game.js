import { sendMessage, timeOutPlayer } from "./app";
import config from "config";
import {Player} from "./db";
const ATTACK_MANA_COST = config.get('damage.manaCost');
const HEAL_MANA_COST = config.get('heal.manaCost');
const REGENERATION_HP_TIME = config.get('regeneration.hp');
const REGENERATION_MP_TIME = config.get('regeneration.mp');

function getPlayer(args) {
    return new Promise(function (resolve, reject) {
        Player.findOneOrCreate(args, function (err, player) {
            if (err !== null || player.length === 0) { reject(err) }
            if (player.hp === 100 && player.mp === 100) {
                resolve(player);
            }
            let currentMP = player.mp;
            let currentHP = player.hp;
            let lastUpdate = player.updated_at;

            if (currentMP < 100) {
                let regeneratedMP = Math.abs(lastUpdate - Math.floor(Date.now() / 1000)) / REGENERATION_MP_TIME;
                currentMP = currentMP + regeneratedMP > 100 ? 100 : currentMP + regeneratedMP;
            }
            if (currentHP < 100) {
                let regeneratedHP = Math.abs(lastUpdate - Math.floor(Date.now() / 1000)) / REGENERATION_HP_TIME;
                currentHP = currentHP + regeneratedHP > 100 ? 100 : currentHP + regeneratedHP;
            }

            Player.findOneAndUpdate(args, { hp: currentHP.toFixed(0), mp: currentMP.toFixed(0), updated_at: Math.floor(Date.now() / 1000) }, { new: true, upsert: true}, function (err, player) {
                console.log("GetPlayer", player);
                resolve(player);
            });
        })
    })
}

function reduceHP(attackingUsername, targetName) {
    const min_damage = config.get('damage.min');
    const max_damage = config.get('damage.max');
    const damage = Math.floor(Math.random() * (max_damage - min_damage + 1)) + min_damage;

    getPlayer({ username: targetName })
        .then(function (player) {

            let currentHP = player.hp;

            if (currentHP <= 0) {
                sendMessage(attackingUsername, `игрок @${targetName} уже убит`);
                return;
            }

            if (currentHP > damage) {
                Player.findOneAndUpdate({ username: targetName }, { hp: player.hp - damage, updated_at: Math.floor(Date.now() / 1000) }, { new: true, upsert: true}, function (err, docs) {
                    console.log("currentHP > damage", docs);
                    reduceMP(attackingUsername, ATTACK_MANA_COST);
                });

                let message = attackingUsername === targetName
                    ? `наносит cебе ${damage} урона. Мазахист!`
                    : `наносит ${damage} урона @${targetName}`;

                sendMessage(attackingUsername, message);
            } else {
                Player.findOneAndUpdate({ username: targetName }, { hp: 0, updated_at: Math.floor(Date.now() / 1000) }, { new: true, upsert: true}, function (err, docs) {
                    console.log("currentHP < damage", docs);
                    timeOutPlayer(targetName);
                    reduceMP(attackingUsername, ATTACK_MANA_COST);
                });
                let message = attackingUsername === targetName
                    ? `убивает cебя нанеся ${damage} урона. Суицид)`
                    : `убивает @${targetName} нанося ${damage} урона`;

                sendMessage(attackingUsername, message);
            }

        })
        .catch()
}

function reduceMP(username, count) {
    getPlayer({ username: username })
        .then(function (player) {
            let currentMP = player.mp;

            Player.findOneAndUpdate({ username: username }, { mp: currentMP - count, updated_at: Math.floor(Date.now() / 1000) }, { new: true, upsert: true}, function (err, docs) {
                console.log("reduceMP", docs);
            });
        });
}

export function attack(attackingUsername, targetName) {
    Player.findOneOrCreate({ username: attackingUsername}, function (err, player) {
        if(player.mp >= ATTACK_MANA_COST) {
            reduceHP(attackingUsername, targetName);
        }  else {
            sendMessage(attackingUsername, `Недостаточно маны. Сейчас у вас ${player.mp} маны. Для атаки используется 10 маны. Регенирация 1мп/3сек`);
        }
    });
}

function increaseHP(healingUsername, targetName) {
    Player.findOneAndUpdate({ username: targetName }, { hp: 100, updated_at: Math.floor(Date.now() / 1000) }, { new: true, upsert: true}, function (err, docs) {
        console.log(docs);
        let message = healingUsername === targetName
            ? `вылечил себя полностью`
            : `${healingUsername} вылечил вас полностью`;

        sendMessage(targetName, message);

    });
}

export function heal(healingUsername, targetName) {
    Player.findOneOrCreate({ username: healingUsername}, function (err, player) {
        if(player.mp >= HEAL_MANA_COST) {
            increaseHP(healingUsername, targetName);
            reduceMP(healingUsername, HEAL_MANA_COST);
        }  else {
            sendMessage(healingUsername, `Недостаточно маны. Сейчас у вас ${player.mp} маны. Для хила используется 50 маны. Регенерация 1мп/3сек`);
        }
    });
}

export function userStatus(username) {
    getPlayer({ username: username })
        .then(function (player) {
            sendMessage(username, `HP: ${player.hp}, MP: ${player.mp}`);
        });
}