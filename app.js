let tmi = require('tmi.js');
import { userStatus, attack, heal } from "./game";
import config from "config";
const CHANNEL = config.get('channel');

var options = {
    options: {
        debug: true
    },
    connection: {
        reconnect: true
    },
    identity: {
        username: config.get('twitch.botname'),
        password: config.get('twitch.oauth')
    },
    channels: [CHANNEL]
};

var client = new tmi.client(options);
client.connect();

client.on("connected", function (address, port) {
    client.action(CHANNEL, `Я хочу сыграть с тобой игру`);
});

// Commands
client.on('chat', function (channel, user, message, self) {

    if (self) return;

    if (user.username === CHANNEL) {
        // Admin commands
    } else {
        // Users commands
        switch (message) {
            case "!status":
                userStatus(user.username);
                break;
            default:
                recognitionCommand(message, user.username);
                break;
        }
    }
});

function recognitionCommand(message, username) {
    if ((message.indexOf('!attack')) !== -1) {
        let splitMSG = message.split(" ");
        let targetName = splitMSG[1].replace('@','');
        attack(username, targetName);
    }

    if ((message.indexOf('!heal')) !== -1) {
        let splitMSG = message.split(" ");
        let targetName = splitMSG[1].replace('@','');
        heal(username, targetName);
    }
}

export function sendMessage(username, message) {
    client.action(CHANNEL, `${username} ${message}`);
}
export function timeOutPlayer(username) {
    client.timeout(CHANNEL, username, 600, "Вы убиты");
}