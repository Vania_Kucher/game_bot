const mongoose = require('mongoose');
import config from 'config';

const dbConfig = {
    'host': config.get('db.host'),
    'username': config.get('db.username'),
    'password': config.get('db.password'),
};
const db_url = `mongodb://${dbConfig.username}:${dbConfig.password}@${dbConfig.host}`;

mongoose.connect(db_url);
let db = mongoose.connection;

db.once('open', function () {
    console.log('DB connected');
});

let playerSchema = new mongoose.Schema({
    username: String,
    hp: { type: Number, default: 100 },
    mp: { type: Number, default: 100 },
    updated_at: { type: Number, default: Math.floor(Date.now() / 1000) },
});

playerSchema.statics.findOneOrCreate = function findOneOrCreate(condition, callback) {
    const self = this;
    self.findOne(condition, (err, result) => {
        return result ? callback(err, result) : self.create(condition, (err, result) => { return callback(err, result) })
    })
};

export let Player = mongoose.model('Player', playerSchema);